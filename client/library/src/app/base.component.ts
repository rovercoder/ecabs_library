import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-base'
})
export class BaseComponent implements OnDestroy {
    
    subscriptions: Subscription[] = [];
    
    ngOnDestroy(): void {
        this.subscriptions.forEach(x => !x.closed && x.unsubscribe());
    }
}
