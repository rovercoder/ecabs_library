import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatCardModule, MatTableModule, MatDatepickerModule, MatSnackBarModule } from '@angular/material';
import { MatMomentDateModule, MatMomentDateAdapterOptions, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { FlexModule, FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { LibraryState } from './store/states/librarystate.state';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LibraryComponent } from './views/library/library.component';
import { AddBookComponent } from './views/library/add-book/add-book.component';
import { BooksListComponent } from './views/library/books-list/books-list.component';

@NgModule({
    declarations: [
        AppComponent,
        LibraryComponent,
        AddBookComponent,
        BooksListComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatSnackBarModule,
        FlexModule,
        FlexLayoutModule,
        NgxsModule.forRoot([LibraryState]),
        NgxsStoragePluginModule.forRoot({ key: 'library._instance' }),
        HttpClientModule
    ],
    providers: [
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
