import { HttpParams } from "@angular/common/http";

export class BookReqResp {
    id?: number;
    author: string;
    title: string;
    published: Date;
    notes: string;
}