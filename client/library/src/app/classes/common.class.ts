
export function getError(data) {
    if (!data)
        return;
    
    let error: string;
    
    if (data.constructor.name.toLowerCase() === "object" && data.error != null) {
        error = data.error;
    } else {
        error = data;
    }

    return error;
}