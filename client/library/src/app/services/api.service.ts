import { Injectable } from "@angular/core";
import { BookReqResp } from "../classes/models.class";
import { _MatTabHeaderMixinBase } from "@angular/material/tabs/typings/tab-header";
import { HttpClient, HttpHeaders } from "@angular/common/http";


@Injectable({
    providedIn: 'root'
})
export class APIService {

    baseUrl: string = 'http://localhost:8080/books';

    constructor(private http: HttpClient) { }

    getBooks() {
        return this.http.get<BookReqResp[]>(this.baseUrl);
    }

    addBook(book: BookReqResp) {
        return this.http.post<number>(this.baseUrl, book, { headers: { 'Content-Type': 'application/json' } });
    }

    getBook(bookID: number) {
        return this.http.get<BookReqResp>(this.baseUrl + "/" + bookID);
    }

}
