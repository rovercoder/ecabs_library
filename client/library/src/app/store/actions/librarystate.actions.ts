import { BookReqResp } from 'src/app/classes/models.class';

export class GetBooks {
    static readonly type = '[Library] GetBooks';
    constructor() {}
}

export class AddBook {
    static readonly type = '[Library] AddBook';
    constructor(public book: BookReqResp) {}
}

export class GetBook {
    static readonly type = '[Library] GetBook';
    constructor(public bookID: number) {}
}

export type LibraryAction = GetBooks | AddBook | GetBook;
