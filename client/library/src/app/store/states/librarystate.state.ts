import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { LibraryStateInterface, Book, initialState } from '../classes/librarystate.classes';
import { GetBooks, AddBook, GetBook } from '../actions/librarystate.actions';
import { APIService } from 'src/app/services/api.service';
import { throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { BookReqResp } from '../../classes/models.class';
import { getError } from 'src/app/classes/common.class';

@State<LibraryStateInterface>({
    name: 'library',
    defaults: initialState
})
export class LibraryState {

    @Selector()
    static _instance(state: LibraryStateInterface) { return state; }

    @Selector()
    static books(state: LibraryStateInterface) {
        return state.books;
    }

    constructor(private store: Store, private apiService: APIService, private snackbar: MatSnackBar) { }

    @Action(GetBooks)
    getBooks({ patchState }: StateContext<LibraryStateInterface>) {
        return this.apiService.getBooks().pipe(tap((books: BookReqResp[]) => {
            patchState({ books: books.map(b => new Book(b)) });
        }), catchError(e => { let error = JSON.stringify(getError(e)); this.snackbar.open(error, null, { duration: 3000 }); return throwError(error); }));
    }

    @Action(AddBook)
    addBook({ getState, patchState }: StateContext<LibraryStateInterface>, { book }: AddBook) {
        const state = getState();
        return this.apiService.addBook(book).pipe(tap((bookID: number) => {
            book.published = <any>book.published.toISOString().slice(0, 10);
            state.books.push(new Book(book, bookID));
            patchState({ lastAddedID: bookID, books: state.books.map(b => b) });
        }), catchError(e => { let error = JSON.stringify(getError(e)); this.snackbar.open(error, null, { duration: 3000 }); return throwError(error); }));
    }

    @Action(GetBook)
    getBook({ patchState }: StateContext<LibraryStateInterface>, { bookID }: GetBook) {
        return this.apiService.getBook(bookID).pipe(tap((book) => {
            patchState({ lastFetchedBook: new Book(book) });
        }), catchError(e => { let error = JSON.stringify(getError(e)); this.snackbar.open(error, null, { duration: 3000 }); return throwError(error); }));
    }
    
}
