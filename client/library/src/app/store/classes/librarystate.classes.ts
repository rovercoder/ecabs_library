import { BookReqResp } from "src/app/classes/models.class";

export class Book {
    ID: number;
    Author: string;
    Title: string;
    Published: Date;
    Notes: string;

    constructor(book: BookReqResp, id?: number) {
        this.ID = id || book.id;
        this.Author = book.author;
        this.Title = book.title;
        this.Published = book.published;
        this.Notes = book.notes;
    }
}

export interface LibraryStateInterface {
    books: Book[];
    lastAddedID: number;
    lastFetchedBook: Book;
}

export const initialState: LibraryStateInterface = {
    books: [],
    lastAddedID: null,
    lastFetchedBook: null
};