import { Component, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/base.component';
import { BookReqResp } from 'src/app/classes/models.class';
import { Store } from '@ngxs/store';
import { AddBook } from 'src/app/store/actions/librarystate.actions';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent extends BaseComponent {

    date: Date = new Date();

    form: BookReqResp = new BookReqResp();
    
    constructor(private store: Store) { super(); }

    addBook() {
        this.subscriptions.push(this.store.dispatch(new AddBook(this.form)).subscribe(() => {
            this.form = <BookReqResp>{};
        }));
    }

}
