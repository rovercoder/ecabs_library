import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base.component';
import { Store, Select } from '@ngxs/store';
import { GetBooks } from 'src/app/store/actions/librarystate.actions';
import { LibraryState } from 'src/app/store/states/librarystate.state';
import { Observable } from 'rxjs';
import { Book } from 'src/app/store/classes/librarystate.classes';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent extends BaseComponent implements OnInit {

    books: Book[] = [];
    @Select(LibraryState.books) books$: Observable<Book[]>;
    columnsToDisplay: string[] = ['id', 'author', 'title', 'published', 'notes'];

    constructor(private store: Store) {
        super();
    }

    ngOnInit(): void {
        this.subscriptions.push(this.books$.subscribe((books) => {
            this.books = books;
        }));
        this.getBooks();
    }
    
    getBooks() {
        this.subscriptions.push(this.store.dispatch(new GetBooks()).subscribe(() => {}));
    }

}