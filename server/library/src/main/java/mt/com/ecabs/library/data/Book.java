package mt.com.ecabs.library.data;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class Book {

    @ApiModelProperty(value = "The book's internal identifier", readOnly = true, example = "1")
    private int id;

    @ApiModelProperty(value = "The book's author", required = true, position = 1, example = "Charles Dickens")
    @NotNull
    @NotBlank
    @Size(min=1, max=255)
    private String author;

    @ApiModelProperty(value = "The book's title", required = true, position = 2, example = "Oliver Twist")
    @NotNull
    @NotBlank
    @Size(min=1, max=255)
    private String title;

    @ApiModelProperty(value = "The book's published date", required = true, position = 3, example = "1837-02-18")
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd", style = "S")
    @PastOrPresent
    private LocalDate published;

    @ApiModelProperty(value = "A comment describing the book's condition and content", required = true, position = 4, example = "Oliver Twist; or, the Parish Boy's " +
            "Progress is Charles Dickens's second novel, and was first published as a serial 1837–39. The story centres on orphan Oliver Twist, born in a workhouse " +
            "and sold into apprenticeship with an undertaker. After escaping, Oliver travels to London, where he meets \"The Artful Dodger\", a member of a gang of " +
            "juvenile pickpockets led by the elderly criminal, Fagin.")
    public String notes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getPublished() {
        return published;
    }

    public void setPublished(LocalDate published) {
        this.published = published;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Book() {}

}