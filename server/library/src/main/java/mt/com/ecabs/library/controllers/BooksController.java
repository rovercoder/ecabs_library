package mt.com.ecabs.library.controllers;

import mt.com.ecabs.library.data.Book;
import mt.com.ecabs.library.data.Books;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksController {

    @CrossOrigin
    @GetMapping()
    public List<Book> getBooks() {
        return Books.get();
    }

    @CrossOrigin
    @GetMapping("{id}")
    public Book getBook(@PathVariable("id") int id) {
        return Books.get(id);
    }

    @CrossOrigin
    @PostMapping()
    public int addBook(@RequestBody @Valid Book book) {
        return Books.add(book);
    }

}
