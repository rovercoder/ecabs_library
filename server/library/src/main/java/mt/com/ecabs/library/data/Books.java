package mt.com.ecabs.library.data;

import mt.com.ecabs.library.helpers.BookNotFoundException;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class Books {
    // Inner class to provide instance of class
    private static class LIST
    {
        private static final List<Book> INSTANCE = Collections.synchronizedList(new ArrayList<>());
    }

    public static List<Book> get()
    {
        return LIST.INSTANCE;
    }

    public static synchronized Book get(int id) {
        return LIST.INSTANCE.stream().filter(x -> x.getId() == id).findFirst().orElseThrow(BookNotFoundException::new);
    }

    public static synchronized int add(Book book) {
        book.setId(LIST.INSTANCE.stream().mapToInt(Book::getId).max().orElse(0) + 1);
        LIST.INSTANCE.add(book);
        return book.getId();
    }
}

